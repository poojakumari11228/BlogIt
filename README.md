# BlogIt
BlogIt, website that will appeal bloggers to write and share blogs &amp; express their ideas/thoughts.

**1. Features**

- Create an account
-  Write blogs
- View all blogs
- Filter your own blogs 
- Delete your blogs
- Add comments
- Delete your comments
- Encrypted Password
- Contact other bloggers

**2. Under Development**

- Notifications
- Categorized Blogs
- Search Blogs
- Blog Ratings
